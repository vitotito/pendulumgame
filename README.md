# Pre installation
 ```
 # Install dependencies (For Ubuntu 18.04)
sudo apt install python3.7 python3.7-venv
 ```

# Installation
```
# Download the project
git clone https://gitlab.com/vitotito/pendulumgame.git

# Go into the folder of the project
cd pendulumgame

# Create a virtualenviroment
python3.7 -m venv venv

# Start the virtualenviroment
source venv/bin/activate

# Install requirements
pip install -r requirements.txt

# Try it!
python main.py
```
